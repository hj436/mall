const path = require('path');
module.exports = {
  //关闭eslint
  lintOnSave: false,
  configureWebpack: {
    resolve: {
      // 别名
      alias: {
        '@': path.resolve(__dirname, './src'),
        // assets: path.resolve(__dirname, './src/assets'),
        // components: path.resolve(__dirname, './src/components'),
        // style: path.resolve(__dirname, './src/style'),
        // utils: path.resolve(__dirname, './src/utils')
      }
    }
  },

  // 配置代理
  devServer: {
    proxy: {
      '/api': {
        target: 'http://39.98.123.211',
        // pathRewrite: { '^/api': ''}
      }
    }
  }

}