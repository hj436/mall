import Vue from 'vue'
import App from './App.vue'
// 引入路由
import router from './router'
// 引入UI框架
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// 引入仓库
import store from '@/store';
// 注册全局组件
import TypeNav from '@/components/TypeNav'
import Pagination from '@/components/Pagination';
// 引入Mock模拟数据
import '@/mock/mockServe'
// 引入所有接口api
import * as API from '@/utils/api'
// 引入图片懒加载
import VueLazyload from 'vue-lazyload'
// 引入自定义插件
import myPlugin from '@/utils/myPlugin'

Vue.use(myPlugin, {
  name: 'upper'
})

Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: require('@/assets/image/error.jpg'),
  loading: require('@/assets/image/loading.jpg'),
  attempt: 1
})

Vue.use(ElementUI);

Vue.component('TypeNav', TypeNav)
Vue.component('Pagination', Pagination)



Vue.config.productionTip = false

new Vue({
  beforeMount() {
    // 注册事件总线
    Vue.prototype.$bus = this
    // 注册全局接口
    Vue.prototype.$API = API
  },
  router,
  store,
  render: h => h(App)
}).$mount('#app')
