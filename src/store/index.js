import Vue from 'vue'
import Vuex from 'vuex'
import ModuleHome from './home'
import ModuleSearcher from './search'
import ModuleDetail from './detail'
import ModuleCart from './cart'
import ModuleUser from './user'
import ModuleCenter from './center'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    ModuleHome,
    ModuleSearcher,
    ModuleDetail,
    ModuleCart,
    ModuleUser,
    ModuleCenter
  }
})