import {getGoodsInfo, addToCart} from '@/utils/api.js'

const state = {
  goodData: {}
}

const mutations = {
  GETGOODSINFO(state, data) {
    state.goodData = data
  }
}

const actions = {
  async DetailInfo({commit}, skuId) {
    let result = await getGoodsInfo(skuId)
    if(result.code === 200) {
      commit('GETGOODSINFO', result.data)
    }
  },

  async addToCart({commit}, {skuId, skuNum}) {
    let result = await addToCart(skuId, skuNum)
    if(result.code === 200) {
      return result.message 
    } else {
      return Promise.reject(new Error(result.message))
    }
  }
  // addToCart({commit}, {skuId, skuNum}) {
  //   let res = addToCart(skuId, skuNum).then((value) => {
  //     console.log(this)
  //     debugger
      
  //     if(value.code === 200) {
  //       this.$message({message: value.message, type: "success"})
  //     }
  //   }, (reason) => {
  //     Promise.reject(new Error(reason.message))
  //   })
  // }
}

const getters = {
  category() {
    return state.goodData.categoryView || {}
  },
  skuInfo() {
    return state.goodData.skuInfo || {}
  },
  spuSaleAttrList() {
    return state.goodData.spuSaleAttrList || []
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}