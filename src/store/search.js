// search仓库
import { getSearchList } from '@/utils/api.js';

const state = {
  searchData: {}
}

const mutations = {
  GETSEARCHLIST(state, data) {
    state.searchData = data
  }
}

const actions = {
  async searchList({ commit }, params = {}) {
    let res = await getSearchList(params)
    if(res.code === 200) {
      commit("GETSEARCHLIST", res.data)
    }
  }
}

const getters = {}

export default {
  state,
  mutations,
  actions,
  getters
}
