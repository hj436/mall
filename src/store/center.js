import {reqMyOrderList} from '@/utils/api'

const state = {
  myOrder: {}
}

const mutations = {
  GETOEDERLISTINFO(state, data) {
    state.myOrder = data
  }
}

const actions = {
  getMyOrderList({commit}, {page, limit}) {
    reqMyOrderList(page, limit).then((res) => {
      commit("GETOEDERLISTINFO", res.data)
    }, (err) => Promise.reject(new Error(err.message)))
  }
}

const getters = {}

export default {
  state,
  mutations,
  actions,
  getters,
}