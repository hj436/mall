// home仓库
import {getBaseCategoryList, getBannerList} from '@/utils/api.js'

const state = {
  categoryList: [],
  bannerList: []
}

const mutations = {
  GETCATEGORYLIST(state, categoryList) {
    state.categoryList = categoryList
  },
  GETBANNERLIST(state, bannerList) {
    state.bannerList = bannerList
  }
}

const actions = {
  // 调用接口拿三级列表数据
  async categoryList ({commit}) { 
    let result = await getBaseCategoryList()
    if(result.code == 200) {
      commit('GETCATEGORYLIST', result.data)
    }
  },

  // 取轮播图数据
  async bannerList({commit}) {
    let res = await getBannerList()
    if(res.code == 200) {
      commit('GETBANNERLIST', res.data)
    }
  }
}

const getters = {}

export default {
  state,
  mutations,
  actions,
  getters
}