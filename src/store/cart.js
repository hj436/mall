import {getUUID} from '@/utils/uuid';
import { reqGetCartList } from '@/utils/api';

const state = {
  uuid: getUUID(),
  cartList: []
}

const mutations = {
  CARTLIST(state, data) {
    state.cartList = data
  }
}

const actions = {
  async getCartList({commit}) {
    let res = await reqGetCartList()
    if(res.code == 200) {
      // 修改原数据的isChecked为后面checked判断好取反
      if(res.data.length) {
          res.data[0].cartInfoList.forEach(e => {
          e.isChecked = true
        })
        return commit("CARTLIST", res.data[0].cartInfoList)
      }
      commit("CARTLIST", [])
    }
  }
}

const getters = {

}

export default {
  state,
  mutations,
  actions,
  getters
}