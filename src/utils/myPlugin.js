let myPlugin = {}

myPlugin.install = (Vue, options) => {
  // 注册全局自定义组件 `v-upper`
  Vue.directive(options.name, (el, params) => {
    console.log(params)
    el.innerHTML = params.value.toUpperCase()
  })
  // 注册一个全局自定义指令 `v-focus`
  Vue.directive('focus', {
    // 当被绑定的元素插入到 DOM 中时……
    inserted: function (el) {
      // 聚焦元素
      el.focus()
    }
  })
}

export default myPlugin