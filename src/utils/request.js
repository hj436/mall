import axios from 'axios'
import nprogress from 'nprogress'
import 'nprogress/nprogress.css'

const requests = axios.create({
  baseUrl: '/api',
  timeout: 5000,
})

// 添加请求拦截器
requests.interceptors.request.use((config) => {
  // 进度条开始
  nprogress.start()
  // 携带token
  config.headers.token = localStorage.getItem('token')
  // 携带临时uuid请求头
  config.headers.userTempId = localStorage.getItem("uuid_token")
  return config
}, (err) => {
  // 对请求错误做些什么
  return Promise.reject(new Error(err.message))
})

// 添加响应拦截器
requests.interceptors.response.use((response) => {
  nprogress.done()
  return response.data
}, (err) => {
  return Promise.reject(new Error(err.message))
})

// 对外暴露
export default requests