import axios from 'axios'
import nprogress from 'nprogress'
import 'nprogress/nprogress.css'

const mockRequests = axios.create({
  baseUrl: '/mock',
  timeout: 5000,
})

// 添加请求拦截器
mockRequests.interceptors.request.use((config) => {
  // 进度条开始
  nprogress.start()
  return config
}, (err) => {
  // 对请求错误做些什么
  return Promise.reject(new Error(err.message))
})

// 添加响应拦截器
mockRequests.interceptors.response.use((response) => {
  nprogress.done()
  return response.data
}, (err) => {
  return Promise.reject(new Error(err.message))
})

// 对外暴露
export default mockRequests