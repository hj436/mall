import requests from './request.js';
import mockRequests from './mockApi.js'

// 三级联动请求地址    /api/product/getBaseCategoryList   ==>  get   无参数
// 发送请求：axios发送请求返回是Promise对象
export const getBaseCategoryList = () => {
  return requests({
    url: '/api/product/getBaseCategoryList',
    method: 'get',
  })
}

// 使用mock发送请求拿banner数据
export const getBannerList = () => {
  return mockRequests({
    url: '/mock/banner',
    method: 'get'
  })
}

// 获取首页商品信息
export const reqHomeGoodList = () => {
  return mockRequests({
    url: '/mock/clothes',
    method: 'get'
  })
}

// 获取搜索的数据 get使用params post使用data
export const getSearchList = (data) => {
  return requests({
    url: '/api/list',
    method: 'post',
    data
  })
}

//获取商品详情页的数据
export const getGoodsInfo = (skuId) => {
  return requests({
    url: `/api/item/${skuId}`,
    method: 'get'
  })
}

// 获取购物车商品列表数据
export const reqGetCartList = () => {
  return requests({
    url: "/api/cart/cartList",
    method: 'get',
  })
}

//添加至购物车 /api/cart/addToCart/{skuId}/{skuNum}
export const addToCart = (skuId, skuNum) => {
  return requests({
    url: `/api/cart/addToCart/${skuId}/${skuNum}`,
    method: 'post',
  })
}

// 选中商品去结算
export const reqCheckedGood = (skuID, isChecked) => {
  return requests({
    url: `/api/cart/checkCart/${skuID}/${isChecked}`,
    method: 'get'
  })
}

// 删除购物车商品
export const reqDeleteCart = (skuId) => {
  return requests({
    url: `/api/cart/deleteCart/${skuId}`,
    method: 'delete'
  })
}

// 获取验证码
export const reqGetCode = (phone) => {
  return requests({
    url: `/api/user/passport/sendCode/${phone}`,
    method: 'get'
  })
}

// 注册用户
export const reqUserRegister = (data) => {
  return requests({
    url: `/api/user/passport/register`,
    method: 'post',
    data
  })
}

// 登录
export const reqLogin = (data) => {
  return requests({
    url: `/api/user/passport/login`,
    method: 'post',
    data
  })
}

// 获取用户信息
export const reqUserInfo = () => {
  return requests({
    url: "/api/user/passport/auth/getUserInfo",
    method: 'get',
  })
}

// 退出登录
export const reqLoginOut = () => {
  return requests({
    url: "/api/user/passport/logout",
    method: 'get'
  })
}

// // 获取用户地址信息(自己注册的账号没有，必须使用13700000000  111111账号密码登录才有)
// export const reqAddress = () => {
//   return requests({
//     url: "/api/user/userAddress/auth/findUserAddressList",
//     method: 'get'
//   })
// }

// 获取用户地址信息
export const reqAddress = () => {
  return mockRequests({
    url: "/mock/address",
    method: 'get'
  })
}

// 获取用户订单交易页信息
export const reqGetTrade = () => {
  return requests({
    url: '/api/order/auth/trade',
    method: 'get'
  })
}

// 提交订单
export const submitOrder = (data) => {
  return requests({
    url: `/api/order/auth/submitOrder?tradeNo=${data.tradeNo}`,
    method: 'post',
    data
  })
}

// 获取订单支付信息
export const reqGetPayInfo = (orderId) => {
  return requests({
    url: `/api/payment/weixin/createNative/${orderId}`,
    method: 'get'
  })
}

// 获取支付订单状态
export const reqPayStatus = (orderId) => {
  return requests({
    url: `/api/payment/weixin/queryPayStatus/${orderId}`,
    method: 'get'
  })
}

// 获取我的订单列表信息
export const reqMyOrderList = (page, limit) => {
  return requests({
    url: `/api/order/auth/${page}/${limit}`,
    method: 'get'
  })
}