const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('@/views/home'),
    meta: { showFooter: true, isShowTypeNav: true },
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/login'),
    meta: { showFooter: true, fBottom: true  },
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('@/views/register'),
    meta: { showFooter: true, fBottom: true },
  },
  {
    path: '/search/:keyword?',
    name: 'Search',
    component: () => import('@/views/search'),
    meta: { showFooter: true, isShowTypeNav: false },
  },
  {
    path: '/detail/:id',
    name: 'Detail',
    component: () => import('@/views/detail'),
    meta: { showFooter: true, isShowTypeNav: true },
  },
  {
    path: '/cart',
    name: 'Cart',
    component: () => import('@/views/ShopCart'),
    meta: { showFooter: true, isShowTypeNav: true, needLogin: true },
  },
  {
    path: '/trade',
    name: 'Trade',
    component: () => import('@/views/Trade'),
    beforeEnter: (to, from, next) => {
      // 路由独享守卫
      if(from.path == '/cart') return next();
      next(from.path);
    },
    meta: { showFooter: true, isShowTypeNav: true, needLogin: true },
  },
  {
    path: '/pay',
    name: 'Pay',
    component: () => import('@/views/Pay'),
    beforeEnter: (to, from, next) => {
      // 路由独享守卫
      if(from.path == '/trade') return next();
      next(from.path);
    },
    meta: { showFooter: true, isShowTypeNav: true, needLogin: true }
  },
  {
    path: '/paySuccess',
    name: 'PaySuccess',
    component: () => import('@/views/PaySuccess'),
    beforeEnter: (to, from, next) => {
      // 路由独享守卫
      if(from.path == '/pay') return next();
      next(from.path);
    },
    meta: { showFooter: true, isShowTypeNav: true, needLogin: true }
  },
  {
    path: '/center',
    name: 'Center',
    component: () => import('@/views/Center'),
    children: [
      {
        path: '/center',
        redirect: '/center/myOrder'
      },
      {
        path: 'myOrder',
        name: 'MyOrder',
        component: () => import('@/views/Center/myOrder'),
        meta: { needLogin: true }
      },
    ],
    meta: { showFooter: true, isShowTypeNav: true, needLogin: true }
  }
]

export default routes